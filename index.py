import pandas as pd
from langdetect import detect
import langdetect
from langdetect import DetectorFactory

# loading the dataframes
df_45off = pd.read_csv('45off.csv')
df_cars_us = pd.read_csv('cars-us.csv')
df_cheap_auto = pd.read_csv('cheap-auto-rentals.csv')
df_lastmin = pd.read_csv('lastmin-flights.csv')
df_row_high = pd.read_csv('ROW-high.csv')
df_row_high = pd.read_csv('ROW-high.csv')
df_row_low = pd.read_csv('ROW-low.csv')
df_row_mob = pd.read_csv('ROW-mob.csv')

# reading sheets from each google's file
# flights
flights_argentina = pd.read_excel('flights_br_arg_mex.xlsx', 'Arge')
flights_brazil = pd.read_excel('flights_br_arg_mex.xlsx', 'brazil')
flights_mexico = pd.read_excel('flights_br_arg_mex.xlsx', 'Mexico')

# cars
cars_argentina = pd.read_excel('car_arg_br_mex.xlsx', 'Arge')
cars_brazil = pd.read_excel('car_arg_br_mex.xlsx', 'Brazil')
cars_mexico = pd.read_excel('car_arg_br_mex.xlsx', 'Mexico')

# creating lists of dataframes per vertical cars\flights
flights_dataframes_list = [df_45off, df_lasmin, df_row_high, df_row_low, df_row_mob]
cars_dataframes_list = [df_cheap_auto, df_cars_us]

#  define the BMM function
def broad_m(query):
    splitted = query.split()
    new_word = ""
    for word in splitted:
        word = " + " + word
        new_word = new_word + word
    return new_word

# define the extracting functions
# flights
def flights_kw_extract(df_list):
    for df in df_list:
        # arg kw
        temp_name = pd.merge(df, flights_argentina, left_on=['Keyword'], right_on=['Query'], how='right')
        temp_name = temp_name[temp_name['Keyword'] != '#NAME?']
        temp_name = temp_name.fillna(value='No match')
        temp_name = temp_name[['Query', 'Keyword']][temp_name['Keyword'] == 'No match']
        # apply keywords match types
        temp_name['broad_m'] = temp_name['Query'].apply(broad_m)
        temp_name['exact'] = temp_name['Query'].apply(lambda x: "[" + x + "]")
        temp_name['phrase'] = temp_name['Query'].apply(lambda x: '"' + x + '"')

        temp_name['language'] = temp_name['Query'].apply(detect)
        temp_name.to_csv(df['Client name'].loc[0] + '_flights_arg.csv', encoding='utf-8-sig', index=False)

        # brazil kw
        temp_name = pd.merge(df, flights_brazil, left_on=['Keyword'], right_on=['Query'], how='right')
        temp_name = temp_name[temp_name['Keyword'] != '#NAME?']
        temp_name = temp_name.fillna(value='No match')
        temp_name = temp_name[['Query', 'Keyword']][temp_name['Keyword'] == 'No match']
        # apply keywords match types
        temp_name['broad_m'] = temp_name['Query'].apply(broad_m)
        temp_name['exact'] = temp_name['Query'].apply(lambda x: "[" + x + "]")
        temp_name['phrase'] = temp_name['Query'].apply(lambda x: '"' + x + '"')

        temp_name['language'] = temp_name['Query'].apply(detect)
        temp_name.to_csv(df['Client name'].loc[0] + '_flights_br.csv', encoding='utf-8-sig', index=False)

        # Mexico kw
        temp_name = pd.merge(df, flights_mexico, left_on=['Keyword'], right_on=['Query'], how='right')
        temp_name = temp_name[temp_name['Keyword'] != '#NAME?']
        temp_name = temp_name.fillna(value='No match')
        temp_name = temp_name[['Query', 'Keyword']][temp_name['Keyword'] == 'No match']
        # apply keywords match types
        temp_name['broad_m'] = temp_name['Query'].apply(broad_m)
        temp_name['exact'] = temp_name['Query'].apply(lambda x: "[" + x + "]")
        temp_name['phrase'] = temp_name['Query'].apply(lambda x: '"' + x + '"')

        temp_name['language'] = temp_name['Query'].apply(detect)
        temp_name.to_csv(df['Client name'].loc[0] + '_flights_mex.csv', encoding='utf-8-sig', index=False)


# cars
def cars_kw_extract(df_list):
    for df in df_list:
        # arg kw
        temp_name = pd.merge(df, cars_argentina, left_on=['Keyword'], right_on=['Query'], how='right')
        temp_name = temp_name[temp_name['Keyword'] != '#NAME?']
        temp_name = temp_name.fillna(value='No match')
        temp_name = temp_name[['Query', 'Keyword']][temp_name['Keyword'] == 'No match']
        # apply keywords match types
        temp_name['broad_m'] = temp_name['Query'].apply(broad_m)
        temp_name['exact'] = temp_name['Query'].apply(lambda x: "[" + x + "]")
        temp_name['phrase'] = temp_name['Query'].apply(lambda x: '"' + x + '"')

        temp_name['language'] = temp_name['Query'].apply(detect)
        temp_name.to_csv(df['Client name'].loc[0] + '_cars_arg.csv', encoding='utf-8-sig', index=False)

        # brazil kw
        temp_name = pd.merge(df, cars_brazil, left_on=['Keyword'], right_on=['Query'], how='right')
        temp_name = temp_name[temp_name['Keyword'] != '#NAME?']
        temp_name = temp_name.fillna(value='No match')
        temp_name = temp_name[['Query', 'Keyword']][temp_name['Keyword'] == 'No match']
        # apply keywords match types
        temp_name['broad_m'] = temp_name['Query'].apply(broad_m)
        temp_name['exact'] = temp_name['Query'].apply(lambda x: "[" + x + "]")
        temp_name['phrase'] = temp_name['Query'].apply(lambda x: '"' + x + '"')

        temp_name['language'] = temp_name['Query'].apply(detect)
        temp_name.to_csv(df['Client name'].loc[0] + '_cars_br.csv', encoding='utf-8-sig', index=False)

        # Mexico kw
        temp_name = pd.merge(df, cars_mexico, left_on=['Keyword'], right_on=['Query'], how='right')
        temp_name = temp_name[temp_name['Keyword'] != '#NAME?']
        temp_name = temp_name.fillna(value='No match')
        temp_name = temp_name[['Query', 'Keyword']][temp_name['Keyword'] == 'No match']
        # apply keywords match types
        temp_name['broad_m'] = temp_name['Query'].apply(broad_m)
        temp_name['exact'] = temp_name['Query'].apply(lambda x: "[" + x + "]")
        temp_name['phrase'] = temp_name['Query'].apply(lambda x: '"' + x + '"')

        temp_name['language'] = temp_name['Query'].apply(detect)
        temp_name.to_csv(df['Client name'].loc[0] + '_cars_mex.csv', encoding='utf-8-sig', index=False)

# running the program
cars_kw_extract(cars_dataframes_list)
flights_kw_extract(flights_dataframes_list)